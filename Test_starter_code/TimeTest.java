package Test_starter_code;
import static org.junit.Assert.*;


import java.sql.Time;
import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
 

@RunWith( Parameterized.class)
public class TimeTest {

	public String timeToTest;

	public TimeTest( String dataTime ) {

	timeToTest = dataTime;

	}
	
	@Parameterized.Parameters
	public static Collection<Object[]> loadData() {
		Object[][] data = {{"12:05:05", 43305}, {"12:05:05", 43503}};
		return Arrays.asList(data);
	}
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetTotalSeconds() {

	//int seconds = Time.getTotalSeconds(this.timeToTest);
	//System.out.println(this.timeToTest);
	//assertTrue( "The seconds were not calculated propertly " , seconds == 43505 );

	}
	@Test
	public void testGetMilliSeconds() {
		//int millis = Time.getMilliSeconds("12:05:05:888");
		//System.out.println(millis);
		//assertTrue("The milliseconds was not calculated properly ", millis == 888);
	}
	@Test(expected = NumberFormatException.class)
	public void testGetMilliSecondsException() {
		//int millis = Time.getMilliSeconds("12:05:05:5555");
		//System.out.println(millis);
		//assertTrue("The milliseconds was not calculated properly ", millis == 5555);
		
	}
	@Test (expected = NumberFormatException.class)
	public void testGetMilliSecondsBoundryIn() {
		//int millis = Time.getMilliSeconds("12:05:05:999");
		//System.out.println(millis);
		//assertTrue("The milliseconds was not calculated properly ", millis == 999);
		
	}
	@Test (expected = NumberFormatException.class)
	public void testGetMilliSecondsBoundryOut() {
		//int millis = Time.getMilliSeconds("12:05:05:1000");
		
	}
	@Test
	public void testGetSeconds() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetTotalMinutes() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetTotalHours() {
		fail("Not yet implemented");
	}

	@Test
	public void testObject() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetClass() {
		fail("Not yet implemented");
	}

	@Test
	public void testHashCode() {
		fail("Not yet implemented");
	}

	@Test
	public void testEquals() {
		fail("Not yet implemented");
	}

	@Test
	public void testClone() {
		fail("Not yet implemented");
	}

	@Test
	public void testToString() {
		fail("Not yet implemented");
	}

	@Test
	public void testNotify() {
		fail("Not yet implemented");
	}

	@Test
	public void testNotifyAll() {
		fail("Not yet implemented");
	}

	@Test
	public void testWait() {
		fail("Not yet implemented");
	}

	@Test
	public void testWaitLong() {
		fail("Not yet implemented");
	}

	@Test
	public void testWaitLongInt() {
		fail("Not yet implemented");
	}

	@Test
	public void testFinalize() {
		fail("Not yet implemented");
	}

}
